package apitools

type PaginatedResponse[T any] struct {
	PageIndex  int `json:"pageIndex"`
	PageSize   int `json:"pageSize"`
	PageCount  int `json:"pageCount"`
	TotalItems int `json:"totalItems"`

	Data []T `json:"data"`
}
