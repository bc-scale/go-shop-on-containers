package apitools

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRequest_Validate(t *testing.T) {
	type fields struct {
		Request    Request
		SortFields []string
	}
	tests := []struct {
		name       string
		fields     fields
		validateFn func(error)
	}{
		{
			name: "ok - with pagination (pageSize)",
			fields: fields{
				Request: Request{
					RequestWithPagination: &RequestWithPagination{
						PageSize: func() *int {
							val := 50
							return &val
						}(),
						PageIndex: nil,
					},
				},
			},
			validateFn: func(err error) {
				assert.NoError(t, err)
			},
		},
		{
			name: "ok - with pagination (pageSize and pageIndex)",
			fields: fields{
				Request: Request{
					RequestWithPagination: &RequestWithPagination{
						PageSize: func() *int {
							val := 50
							return &val
						}(),
						PageIndex: func() *int {
							val := 2
							return &val
						}(),
					},
				},
			},
			validateFn: func(err error) {
				assert.NoError(t, err)
			},
		},
		{
			name: "invalid pageSize",
			fields: fields{
				Request: Request{
					RequestWithPagination: &RequestWithPagination{
						PageSize: func() *int {
							val := -50
							return &val
						}(),
					},
				},
			},
			validateFn: func(err error) {
				assert.EqualError(t, err, "pagination: invalid pageSize; must be > 0 and <= 100")
			},
		},
		{
			name: "invalid pageIndex",
			fields: fields{
				Request: Request{
					RequestWithPagination: &RequestWithPagination{
						PageIndex: func() *int {
							val := -2
							return &val
						}(),
					},
				},
			},
			validateFn: func(err error) {
				assert.EqualError(t, err, "pagination: invalid pageIndex; must be > 0")
			},
		},
		{
			name: "ok - with sorting",
			fields: fields{
				Request: Request{
					RequestWithSorting: &RequestWithSorting{
						SortField: func() *string {
							val := "testField"
							return &val
						}(),
					},
				},
				SortFields: []string{"testField"},
			},
			validateFn: func(err error) {
				assert.NoError(t, err)
			},
		},
		{
			name: "ok - with sorting and order",
			fields: fields{
				Request: Request{
					RequestWithSorting: &RequestWithSorting{
						SortField: func() *string {
							val := "testField"
							return &val
						}(),
						SortOrder: func() *string {
							val := "desc"
							return &val
						}(),
					},
				},
				SortFields: []string{"testField"},
			},
			validateFn: func(err error) {
				assert.NoError(t, err)
			},
		},
		{
			name: "invalid sorting field",
			fields: fields{
				Request: Request{
					RequestWithSorting: &RequestWithSorting{
						SortField: func() *string {
							val := "price"
							return &val
						}(),
					},
				},
			},
			validateFn: func(err error) {
				assert.EqualError(t, err, "sortField: price not supported")
			},
		},
		{
			name: "sorting field not provided",
			fields: fields{
				Request: Request{
					RequestWithSorting: &RequestWithSorting{
						SortField: nil,
						SortOrder: func() *string {
							val := "descending"
							return &val
						}(),
					},
				},
				SortFields: []string{"salePrice"},
			},
			validateFn: func(err error) {
				assert.EqualError(t, err, "sortField: must be provided along with sortOrder")
			},
		},
		{
			name: "invalid sorting order",
			fields: fields{
				Request: Request{
					RequestWithSorting: &RequestWithSorting{
						SortField: func() *string {
							val := "salePrice"
							return &val
						}(),
						SortOrder: func() *string {
							val := "descending"
							return &val
						}(),
					},
				},
				SortFields: []string{"salePrice"},
			},
			validateFn: func(err error) {
				assert.EqualError(t, err, "sortOrder: descending not supported; must be asc or desc")
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var req Request
			if tt.fields.Request.RequestWithPagination != nil {
				req.RequestWithPagination = tt.fields.Request.RequestWithPagination
			}
			if tt.fields.Request.RequestWithSorting != nil {
				req.RequestWithSorting = tt.fields.Request.RequestWithSorting
			}

			var sortFields []string
			if len(tt.fields.SortFields) > 0 {
				sortFields = tt.fields.SortFields
			}
			tt.validateFn(req.Validate(sortFields...))
		})
	}
}

func TestRequest_SortParams(t *testing.T) {
	type fields struct {
		PageSize  *int
		PageIndex *int
		SortField *string
		SortOrder *string
	}
	type args struct {
		allowedFields []string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
		want1  string
		want2  bool
	}{
		{
			name: "ok",
			fields: fields{
				SortField: func() *string {
					val := "salePrice"
					return &val
				}(),
				SortOrder: nil,
			},
			args: args{
				allowedFields: []string{"salePrice"},
			},
			want:  "sale_price",
			want1: "ASC",
			want2: true,
		},
		{
			name: "ok - with sortOrder",
			fields: fields{
				SortField: func() *string {
					val := "salePrice"
					return &val
				}(),
				SortOrder: func() *string {
					val := "desc"
					return &val
				}(),
			},
			args: args{
				allowedFields: []string{"salePrice"},
			},
			want:  "sale_price",
			want1: "DESC",
			want2: true,
		},
		{
			name: "invalid sortField",
			fields: fields{
				SortField: func() *string {
					val := "price"
					return &val
				}(),
			},
			args: args{
				allowedFields: []string{},
			},
			want:  "",
			want1: "",
			want2: false,
		},
		{
			name: "ok - allowed field",
			fields: fields{
				SortField: func() *string {
					val := "testField"
					return &val
				}(),
			},
			args: args{
				allowedFields: []string{"testField"},
			},
			want:  "test_field",
			want1: "ASC",
			want2: true,
		},
		{
			name: "invalid sortOrder",
			fields: fields{
				SortField: func() *string {
					val := "salePrice"
					return &val
				}(),
				SortOrder: func() *string {
					val := "descending"
					return &val
				}(),
			},
			args: args{
				allowedFields: []string{},
			},
			want:  "",
			want1: "",
			want2: false,
		},
		{
			name: "unitsSoldLast7Days",
			fields: fields{
				SortField: func() *string {
					val := "unitsSoldLast7Days"
					return &val
				}(),
			},
			args: args{
				allowedFields: []string{"unitsSoldLast7Days"},
			},
			want:  "units_sold_last_7_days",
			want1: "ASC",
			want2: true,
		},
		{
			name: "unitsSoldLast180Days",
			fields: fields{
				SortField: func() *string {
					val := "unitsSoldLast180Days"
					return &val
				}(),
				SortOrder: func() *string {
					val := "desc"
					return &val
				}(),
			},
			args: args{
				allowedFields: []string{"unitsSoldLast180Days"},
			},
			want:  "units_sold_last_180_days",
			want1: "DESC",
			want2: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			req := &Request{
				RequestWithPagination: &RequestWithPagination{
					PageSize:  tt.fields.PageSize,
					PageIndex: tt.fields.PageIndex,
				},
				RequestWithSorting: &RequestWithSorting{
					SortField: tt.fields.SortField,
					SortOrder: tt.fields.SortOrder,
				},
			}
			got, got1, got2 := req.SortParams(tt.args.allowedFields...)
			if got != tt.want {
				t.Errorf("Request.sortParams() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("Request.sortParams() got1 = %v, want %v", got1, tt.want1)
			}
			if got2 != tt.want2 {
				t.Errorf("Request.sortParams() got2 = %v, want %v", got2, tt.want2)
			}
		})
	}
}
