package apitools

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPaginator(t *testing.T) {
	type args struct {
		pageSize  *int
		pageIndex *int
	}
	tests := []struct {
		name       string
		args       args
		validateFn func(Paginator, error)
	}{
		{
			name: "ok (default values)",
			args: args{
				pageSize:  nil,
				pageIndex: nil,
			},
			validateFn: func(p Paginator, err error) {
				assert.NoError(t, err)
				assert.Equal(t, 100, p.PageSize())
				assert.Equal(t, 1, p.PageIndex())
				assert.Equal(t, 2, p.NextPageIndex(200, 100))
			},
		},
		{
			name: "ok (custom pageIndex; default pageSize)",
			args: args{
				pageSize: nil,
				pageIndex: func() *int {
					val := 2
					return &val
				}(),
			},
			validateFn: func(p Paginator, err error) {
				assert.NoError(t, err)
				assert.Equal(t, 100, p.PageSize())
				assert.Equal(t, 2, p.PageIndex())
				assert.Equal(t, 3, p.NextPageIndex(300, 100))
			},
		},
		{
			name: "ok (custom pageSize; default pageIndex)",
			args: args{
				pageSize: func() *int {
					val := 50
					return &val
				}(),
				pageIndex: nil,
			},
			validateFn: func(p Paginator, err error) {
				assert.NoError(t, err)
				assert.Equal(t, 50, p.PageSize())
				assert.Equal(t, 1, p.PageIndex())
				assert.Equal(t, 2, p.NextPageIndex(100, 50))
			},
		},
		{
			name: "ok (custom pageSize and pageIndex)",
			args: args{
				pageSize: func() *int {
					val := 50
					return &val
				}(),
				pageIndex: func() *int {
					val := 2
					return &val
				}(),
			},
			validateFn: func(p Paginator, err error) {
				assert.NoError(t, err)
				assert.Equal(t, 50, p.PageSize())
				assert.Equal(t, 2, p.PageIndex())
				assert.Equal(t, 3, p.NextPageIndex(150, 50))
			},
		},
		{
			name: "ok (results only for one page)",
			args: args{
				pageSize:  nil,
				pageIndex: nil,
			},
			validateFn: func(p Paginator, err error) {
				assert.NoError(t, err)
				assert.Equal(t, 100, p.PageSize())
				assert.Equal(t, 1, p.PageIndex())
				assert.Equal(t, 0, p.NextPageIndex(100, 100))
			},
		},
		{
			name: "ok (results only for two pages)",
			args: args{
				pageSize: nil,
				pageIndex: func() *int {
					val := 2
					return &val
				}(),
			},
			validateFn: func(p Paginator, err error) {
				assert.NoError(t, err)
				assert.Equal(t, 100, p.PageSize())
				assert.Equal(t, 2, p.PageIndex())
				assert.Equal(t, 0, p.NextPageIndex(200, 100))
			},
		},
		{
			name: "ok (no results)",
			args: args{
				pageSize:  nil,
				pageIndex: nil,
			},
			validateFn: func(p Paginator, err error) {
				assert.NoError(t, err)
				assert.Equal(t, 100, p.PageSize())
				assert.Equal(t, 1, p.PageIndex())
				assert.Equal(t, 0, p.NextPageIndex(0, 0))
			},
		},
		{
			name: "ok (results < pageSize)",
			args: args{
				pageSize:  nil,
				pageIndex: nil,
			},
			validateFn: func(p Paginator, err error) {
				assert.NoError(t, err)
				assert.Equal(t, 100, p.PageSize())
				assert.Equal(t, 1, p.PageIndex())
				assert.Equal(t, 0, p.NextPageIndex(50, 50))
			},
		},
		{
			name: "invalid pageIndex; valid pageSize",
			args: args{
				pageSize: func() *int {
					val := 50
					return &val
				}(),
				pageIndex: func() *int {
					val := -1
					return &val
				}(),
			},
			validateFn: func(p Paginator, err error) {
				assert.EqualError(t, err, "invalid pageIndex; must be > 0")
			},
		},
		{
			name: "invalid pageSize; valid pageIndex",
			args: args{
				pageSize: func() *int {
					val := -50
					return &val
				}(),
				pageIndex: func() *int {
					val := 1
					return &val
				}(),
			},
			validateFn: func(p Paginator, err error) {
				assert.EqualError(t, err, "invalid pageSize; must be > 0 and <= 100")
			},
		},
		{
			name: "invalid pageSize; valid pageIndex",
			args: args{
				pageSize: func() *int {
					val := 150
					return &val
				}(),
				pageIndex: func() *int {
					val := 1
					return &val
				}(),
			},
			validateFn: func(p Paginator, err error) {
				assert.EqualError(t, err, "invalid pageSize; must be > 0 and <= 100")
			},
		},
		{
			name: "invalid pageSize and pageIndex",
			args: args{
				pageSize: func() *int {
					val := 150
					return &val
				}(),
				pageIndex: func() *int {
					val := -30
					return &val
				}(),
			},
			validateFn: func(p Paginator, err error) {
				assert.EqualError(t, err, "invalid pageSize; must be > 0 and <= 100")
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewPaginator(tt.args.pageSize, tt.args.pageIndex)
			tt.validateFn(got, err)
		})
	}
}
