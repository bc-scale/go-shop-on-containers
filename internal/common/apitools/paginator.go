package apitools

import (
	"errors"
	"math"
)

type Paginator struct {
	pageSize  int
	pageIndex int
}

func NewPaginator(pageSize *int, pageIndex *int) (Paginator, error) {
	const (
		minPageSize = 1
		maxPageSize = 100

		defaultPageSize  = maxPageSize
		defaultPageIndex = 1
	)

	p := Paginator{
		pageSize:  defaultPageSize,
		pageIndex: defaultPageIndex,
	}

	if pageSize != nil {
		if *pageSize < minPageSize || *pageSize > maxPageSize {
			return Paginator{}, errors.New("invalid pageSize; must be > 0 and <= 100")
		}

		p.pageSize = *pageSize
	}

	if pageIndex != nil {
		if *pageIndex <= 0 {
			return Paginator{}, errors.New("invalid pageIndex; must be > 0")
		}

		p.pageIndex = *pageIndex
	}
	return p, nil
}

func (p Paginator) PageSize() int {
	return p.pageSize
}

func (p Paginator) PageIndex() int {
	return p.pageIndex
}

func (p Paginator) PagesCount(totalCount int) int {
	return int(math.Ceil(float64(totalCount) / float64(p.pageSize)))
}

func (p Paginator) NextPageIndex(totalCount, returnedCount int) int {
	nextPage := p.pageIndex + 1
	totalPages := p.PagesCount(totalCount)

	if totalCount == 0 || returnedCount < p.pageSize || totalPages == p.pageIndex {
		return 0
	}
	return nextPage
}
