package apitools

import (
	"strings"
	"unicode"

	"gitlab.com/gnsky/go-shop-on-containers/internal/common/errors"
)

// Request provides parameters for pagination and sorting.
type Request struct {
	*RequestWithPagination
	*RequestWithSorting
}

func (req *Request) Validate(sortFields ...string) error {
	if req.RequestWithPagination != nil {
		if err := req.RequestWithPagination.Validate(); err != nil {
			return err
		}
	}

	if req.RequestWithSorting == nil {
		return nil
	}
	return req.RequestWithSorting.Validate(sortFields...)
}

func (req *Request) SortParams(sortFields ...string) (string, string, bool) {
	return req.RequestWithSorting.SortParams(sortFields...)
}

type RequestWithPagination struct {
	PageIndex *int
	PageSize  *int
}

func (req *RequestWithPagination) Validate() error {
	if _, err := NewPaginator(req.PageSize, req.PageIndex); err != nil {
		return errors.Newf("pagination: %w", err)
	}
	return nil
}

const (
	AscendingSortOrder  = "asc"
	DescendingSortOrder = "desc"
)

type RequestWithSorting struct {
	SortField *string
	SortOrder *string
}

func (req *RequestWithSorting) Validate(allowedSortFields ...string) error {
	if req.SortField == nil && req.SortOrder != nil {
		return errors.New("sortField: must be provided along with sortOrder")
	}

	if req.SortField != nil {
		if err := req.validateSortField(*req.SortField, allowedSortFields...); err != nil {
			return errors.Newf("sortField: %w", err)
		}
	}

	if req.SortOrder != nil {
		if err := req.validateSortOrder(*req.SortOrder); err != nil {
			return errors.Newf("sortOrder: %w", err)
		}
	}
	return nil
}

func (req *RequestWithSorting) SortParams(allowedFields ...string) (sortField, sortOrder string, isEnabled bool) {
	if req.SortField == nil {
		return sortField, sortOrder, isEnabled
	}
	if err := req.validateSortField(*req.SortField, allowedFields...); err != nil {
		return "", "", false
	}

	sortField = *req.SortField
	sortOrder = "ASC"
	isEnabled = true

	if req.SortOrder != nil {
		if err := req.validateSortOrder(*req.SortOrder); err != nil {
			return "", "", false
		}

		sortOrder = strings.ToUpper(*req.SortOrder)
	}
	return req.toSnakeCase(sortField), sortOrder, isEnabled
}

func (req *RequestWithSorting) validateSortOrder(order string) error {
	switch order {
	case AscendingSortOrder, DescendingSortOrder:
		return nil
	default:
		return errors.Newf("%s not supported; must be %s or %s",
			order, AscendingSortOrder, DescendingSortOrder)
	}
}

func (req *RequestWithSorting) validateSortField(field string, allowedFields ...string) error {
	for _, allowedField := range allowedFields {
		if field == allowedField {
			return nil
		}
	}
	return errors.Newf("%s not supported", field)
}

func (req *RequestWithSorting) toSnakeCase(str string) string {
	var result []rune
	for idx, ch := range str {
		if unicode.IsUpper(ch) || unicode.IsDigit(ch) && !unicode.IsDigit(rune(str[idx-1])) {
			result = append(result, '_')
			result = append(result, unicode.ToLower(ch))
		} else {
			result = append(result, ch)
		}
	}
	return string(result)
}
