package errors

import "fmt"

type ValidationError struct {
	message string
}

func NewValidationError(err error) *ValidationError {
	return &ValidationError{message: err.Error()}
}

func NewValidationErrorWithMessage(message string, args ...any) *ValidationError {
	return &ValidationError{message: fmt.Sprintf(message, args...)}
}

func (err *ValidationError) Error() string {
	return err.message
}

type NotFoundError struct {
	entityID string
}

func NewNotFoundError(entityID string) *NotFoundError {
	return &NotFoundError{entityID: entityID}
}

func (err *NotFoundError) Error() string {
	return fmt.Sprintf("entity (ID: %s) not found",
		err.entityID)
}

type AlreadyExistsError struct {
	entityID string
}

func NewAlreadyExistsError(entityID string) *AlreadyExistsError {
	return &AlreadyExistsError{entityID: entityID}
}

func (err *AlreadyExistsError) Error() string {
	return fmt.Sprintf("entity (ID: %s) already exists",
		err.entityID)
}

type InternalError struct {
	internalErr error
}

func NewInternalError(err error) *InternalError {
	return &InternalError{internalErr: err}
}

func (err *InternalError) Error() string {
	return fmt.Sprintf("internal error occured: %s",
		err.internalErr.Error())
}

func (err *InternalError) Unwrap() error {
	return err.internalErr
}
