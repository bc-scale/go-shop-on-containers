package errors

import (
	"errors"
	"fmt"
)

func New(text string) error {
	return errors.New(text)
}

func Newf(format string, args ...any) error {
	return fmt.Errorf(format, args...)
}

func Is(err, target error) bool {
	return errors.Is(err, target)
}

func As(err error, target any) bool {
	return errors.As(err, &target)
}

func Unwrap(err error) error {
	return errors.Unwrap(err)
}

func Wrap(wrapper, err error) error {
	if wrapper == nil {
		return nil
	}
	if err == nil {
		return wrapper
	}
	return &wrappedError{wrapper: wrapper, err: err}
}

type wrappedError struct {
	wrapper error
	err     error
}

func (e *wrappedError) Is(err error) bool {
	return errors.Is(e.wrapper, err)
}

func (e *wrappedError) As(err any) bool {
	return errors.As(e.wrapper, &err)
}

func (e *wrappedError) Unwrap() error {
	return e.err
}

func (e *wrappedError) Error() string {
	return fmt.Sprintf("%s: %s", e.wrapper, e.err)
}
