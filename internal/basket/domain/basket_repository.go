package domain

import (
	"context"
)

type BasketRepository interface {
	FindOne(ctx context.Context, customerID string) (Basket, error)
	Save(ctx context.Context, basket Basket) error
	Delete(ctx context.Context, customerID string) error
}
