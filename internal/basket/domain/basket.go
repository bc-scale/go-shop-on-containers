package domain

type Basket struct {
	CustomerID string
	Items      []BasketItem
}

type BasketItem struct {
	ProductID string
	Quantity  int
}
