package v1http

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/gnsky/go-shop-on-containers/internal/basket/app"
)

func RegisterEndpoints(router gin.IRouter, basketSvc app.BasketService) error {
	if router == nil {
		return errors.New("gin.IRouter not provided")
	}
	if basketSvc == nil {
		return errors.New("app.BasketService not provided")
	}

	router.GET("/api/v1/basket", GetBasket(basketSvc))
	router.POST("/api/v1/basket/add-item", AddBasketItem(basketSvc))
	router.POST("/api/v1/basket/remove-item", RemoveBasketItem(basketSvc))
	router.POST("/api/v1/basket/checkout", Checkout(basketSvc))
	return nil
}

func GetBasket(basketSvc app.BasketService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.Status(http.StatusNotImplemented)
	}
}

func AddBasketItem(basketSvc app.BasketService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.Status(http.StatusNotImplemented)
	}
}

func RemoveBasketItem(basketSvc app.BasketService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.Status(http.StatusNotImplemented)
	}
}

func Checkout(basketSvc app.BasketService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.Status(http.StatusNotImplemented)
	}
}
