package app

import (
	"context"

	"gitlab.com/gnsky/go-shop-on-containers/internal/common/errors"

	"github.com/jinzhu/copier"

	"gitlab.com/gnsky/go-shop-on-containers/internal/basket/domain"
)

type BasketService interface {
	FindCustomerBasket(ctx context.Context, customerID string) (BasketResponse, error)
	AddItemToBasket(ctx context.Context, req AddItemToBasketRequest) error
	RemoveItemFromBasket(ctx context.Context, req RemoveItemFromBasketRequest) error
	Checkout(ctx context.Context, req BasketCheckoutRequest) error
}

type BasketServiceImpl struct {
	basketRepo domain.BasketRepository
}

func (svc *BasketServiceImpl) FindCustomerBasket(ctx context.Context, customerID string) (BasketResponse, error) {
	basket, err := svc.basketRepo.FindOne(ctx, customerID)
	if err != nil {
		return BasketResponse{}, err
	}

	var basketResp BasketResponse
	if err = copier.Copy(&basketResp, &basket); err != nil {
		return BasketResponse{}, errors.NewInternalError(err)
	}
	return basketResp, nil
}

func (svc *BasketServiceImpl) AddItemToBasket(ctx context.Context, req AddItemToBasketRequest) error {
	basket, err := svc.basketRepo.FindOne(ctx, req.CustomerID)
	if err != nil {
		return err
	}

	for _, item := range basket.Items {
		if item.ProductID == req.ProductID {
			return errors.NewAlreadyExistsError(req.ProductID)
		}
	}

	basket.Items = append(basket.Items, domain.BasketItem{
		ProductID: req.ProductID,
		Quantity:  req.Quantity,
	})
	return svc.basketRepo.Save(ctx, basket)
}

func (svc *BasketServiceImpl) RemoveItemFromBasket(ctx context.Context, req RemoveItemFromBasketRequest) error {
	basket, err := svc.basketRepo.FindOne(ctx, req.CustomerID)
	if err != nil {
		return err
	}

	for idx, item := range basket.Items {
		if item.ProductID == req.ProductID {
			basket.Items = append(basket.Items[:idx], basket.Items[idx+1:]...)
			return svc.basketRepo.Save(ctx, basket)
		}
	}
	return errors.NewNotFoundError(req.ProductID)
}

func (svc *BasketServiceImpl) Checkout(ctx context.Context, req BasketCheckoutRequest) error {
	//TODO implement me
	panic("implement me")
}
