package app

type BasketResponse struct {
	CustomerID string               `json:"customerID"`
	Items      []BasketItemResponse `json:"basketItems"`
}

type BasketItemResponse struct {
	ProductID string `json:"productOD"`
	Quantity  int    `json:"quantity"`
}
