package app

import "time"

type AddItemToBasketRequest struct {
	CustomerID string `json:"customerID"`
	ProductID  string `json:"productID"`
	Quantity   int    `json:"quantity"`
}

type RemoveItemFromBasketRequest struct {
	CustomerID string `json:"customerID"`
	ProductID  string `json:"productID"`
}

type BasketCheckoutRequest struct {
	BuyerID   string `json:"buyerID"`
	BuyerName string `json:"buyerName"`

	Country string `json:"country"`
	City    string `json:"city"`
	State   string `json:"state"`
	Street  string `json:"street"`
	ZipCode string `json:"zipCode"`

	CardTypeID         int       `json:"cardTypeID"`
	CardNumber         string    `json:"cardNumber"`
	CardHolderName     string    `json:"cardHolderName"`
	CardExpiration     time.Time `json:"cardExpiration"`
	CardSecurityNumber string    `json:"cardSecurityNumber"`
}
