package v1http

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/gnsky/go-shop-on-containers/internal/catalog/app"
	"gitlab.com/gnsky/go-shop-on-containers/internal/common/errors"
)

func RegisterEndpoints(router gin.IRouter, catalogSvc app.CatalogService) error {
	if router == nil {
		return errors.New("gin.IRouter not provided")
	}
	if catalogSvc == nil {
		return errors.New("app.CatalogService not provided")
	}
	return nil
}
