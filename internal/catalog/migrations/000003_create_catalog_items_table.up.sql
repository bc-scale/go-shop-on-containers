CREATE TABLE IF NOT EXISTS "catalog_items"
(
    "id"                  INTEGER NOT NULL PRIMARY KEY,
    "name"                VARCHAR NOT NULL,
    "description"         VARCHAR,
    "price"               DECIMAL NOT NULL,
    "picture_name"        VARCHAR,

    "catalog_type_id"     INTEGER REFERENCES catalog_types ("id") ON DELETE SET NULL,
    "catalog_brand_id"    INTEGER REFERENCES catalog_brands ("id") ON DELETE SET NULL,

    "available_stock"     INTEGER NOT NULL DEFAULT 0,
    "restock_threshold"   INTEGER NOT NULL,
    "max_stock_threshold" INTEGER NOT NULL,
    "on_reorder"          BOOLEAN NOT NULL DEFAULT FALSE
);
