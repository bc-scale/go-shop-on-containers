package domain

import "fmt"

type CatalogBrand struct {
	ID    string
	Brand string
}

func (c CatalogBrand) String() string {
	return fmt.Sprintf("CatalogBrand(ID: %s; Brand: %s)", c.ID, c.Brand)
}
