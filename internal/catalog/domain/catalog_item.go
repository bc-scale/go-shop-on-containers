package domain

import (
	"fmt"

	"gitlab.com/gnsky/go-shop-on-containers/internal/common/errors"

	"github.com/shopspring/decimal"
)

type CatalogItem struct {
	ID          string
	Name        string
	Description string

	Price       decimal.Decimal
	PictureName string

	CatalogType  CatalogType
	CatalogBrand CatalogBrand

	// Quantity in stock
	AvailableStock int

	// Available stock at which we should reorder
	RestockThreshold int

	// Maximum number of units that can be in-stock at any time
	// (due to physical/logistical constraints in warehouses)
	MaxStockThreshold int
	OnReorder         bool
}

// RemoveStock decrements the quantity of a particular item in inventory and ensures the restockThreshold hasn't
// been breached. If so, a RestockRequest is generated in CheckThreshold.
//
// If there is sufficient stock of an item, then the integer returned at the end of this call
// should be the same as quantityDesired.
//
// In the event that there is not sufficient stock available,
// the method will remove whatever stock is available and return that quantity to the client.
// In this case, it is the responsibility of the client to determine
// if the amount that is returned is the same as quantityDesired.
// It is invalid to pass in a negative number.
//
// Returns the number actually removed from stock.
func (c CatalogItem) RemoveStock(quantityDesired int) (int, error) {
	if c.AvailableStock == 0 {
		return 0, errors.NewValidationErrorWithMessage("Empty stock, product item %s is sold out", c.Name)
	}
	if quantityDesired <= 0 {
		return 0, errors.NewValidationErrorWithMessage("Item units desired should be greater than zero")
	}

	removed := quantityDesired
	if c.AvailableStock < quantityDesired {
		removed = c.AvailableStock
	}

	c.AvailableStock -= removed
	return removed, nil
}

// AddStock increments the quantity of a particular item in inventory.
// Returns the quantity that has been added to stock.
func (c *CatalogItem) AddStock(quantity int) int {
	original := c.AvailableStock

	// The quantity that the client is trying to add to stock is greater than
	// what can be physically accommodated in the Warehouse
	if c.AvailableStock+quantity > c.MaxStockThreshold {
		// For now, this method only adds new units up maximum stock threshold.
		// In an expanded version of this application, we could include tracking for the remaining units
		// and store information about overstock elsewhere.
		c.AvailableStock += c.MaxStockThreshold - c.AvailableStock
	} else {
		c.AvailableStock += quantity
	}

	c.OnReorder = false
	return c.AvailableStock - original
}

func (c CatalogItem) String() string {
	return fmt.Sprintf("CatalogItem(ID: %s; Name: %s; Price: %s; AvailableStock: %d)",
		c.ID, c.Name, c.Price, c.AvailableStock)
}
