package domain

import "fmt"

type CatalogType struct {
	ID   string
	Type string
}

func (c CatalogType) String() string {
	return fmt.Sprintf("CatalogType(ID: %s; Type: %s)", c.ID, c.Type)
}
