package app

import "github.com/shopspring/decimal"

type CatalogType struct {
	ID   string `json:"id"`
	Type string `json:"catalogType"`
}

type CatalogBrand struct {
	ID    string `json:"id"`
	Brand string `json:"catalogBrand"`
}

type CatalogItem struct {
	ID          string `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`

	Price       decimal.Decimal `json:"price"`
	PictureName string          `json:"pictureName"`

	CatalogType  CatalogType  `json:"catalogType"`
	CatalogBrand CatalogBrand `json:"catalogBrand"`

	AvailableStock    int  `json:"availableStock"`
	RestockThreshold  int  `json:"restockThreshold"`
	MaxStockThreshold int  `json:"maxStockThreshold"`
	OnReorder         bool `json:"onReorder"`
}
