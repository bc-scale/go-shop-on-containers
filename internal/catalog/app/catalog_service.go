package app

import (
	"context"
	"database/sql"

	. "github.com/volatiletech/sqlboiler/v4/queries/qm"

	"gitlab.com/gnsky/go-shop-on-containers/internal/common/errors"

	"gitlab.com/gnsky/go-shop-on-containers/internal/catalog/infrastructure/models"

	"gitlab.com/gnsky/go-shop-on-containers/internal/common/apitools"
)

type CatalogService interface {
	FindCatalogTypes(
		ctx context.Context,
		req apitools.RequestWithPagination,
	) (*apitools.PaginatedResponse[CatalogType], error)

	FindCatalogBrands(
		ctx context.Context,
		req apitools.RequestWithPagination,
	) (*apitools.PaginatedResponse[CatalogBrand], error)

	FindCatalogItems(
		ctx context.Context,
		filter CatalogItemFilter,
	) (*apitools.PaginatedResponse[CatalogItem], error)
}

var (
	// assert that CatalogServiceImpl implements CatalogService.
	_ CatalogService = (*CatalogServiceImpl)(nil)
)

type CatalogServiceImpl struct {
	dbClient *sql.DB
}

func NewCatalogService(dbClient *sql.DB) (*CatalogServiceImpl, error) {
	if dbClient == nil {
		return nil, errors.New("sql.DB not provided")
	}
	return &CatalogServiceImpl{dbClient: dbClient}, nil
}

func (svc *CatalogServiceImpl) FindCatalogTypes(
	ctx context.Context,
	req apitools.RequestWithPagination,
) (*apitools.PaginatedResponse[CatalogType], error) {
	if err := req.Validate(); err != nil {
		return nil, errors.NewValidationError(err)
	}

	paginator, err := apitools.NewPaginator(req.PageSize, req.PageIndex)
	if err != nil {
		return nil, errors.NewValidationErrorWithMessage("paginator: %w", err)
	}

	catalogTypes, err := models.CatalogTypes(
		Limit(paginator.PageSize()),
		Offset(paginator.PageIndex()),
	).All(ctx, svc.dbClient)

	if err != nil {
		return nil, errors.NewInternalError(err)
	}

	_ = catalogTypes
	return nil, nil
}

func (svc *CatalogServiceImpl) FindCatalogBrands(
	ctx context.Context,
	req apitools.RequestWithPagination,
) (*apitools.PaginatedResponse[CatalogBrand], error) {
	//TODO implement me
	panic("implement me")
}

func (svc *CatalogServiceImpl) FindCatalogItems(
	ctx context.Context,
	filter CatalogItemFilter,
) (*apitools.PaginatedResponse[CatalogItem], error) {
	//TODO implement me
	panic("implement me")
}
