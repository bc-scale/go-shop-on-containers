package app

import "gitlab.com/gnsky/go-shop-on-containers/internal/common/apitools"

type CatalogItemFilter struct {
	apitools.Request

	ItemName     *string
	CatalogType  *string
	CatalogBrand *string
}
