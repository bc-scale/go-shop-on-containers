// Code generated by SQLBoiler 4.11.0 (https://github.com/volatiletech/sqlboiler). DO NOT EDIT.
// This file is meant to be re-generated in place and/or deleted at any time.

package models

import (
	"bytes"
	"context"
	"reflect"
	"testing"

	"github.com/volatiletech/randomize"
	"github.com/volatiletech/sqlboiler/v4/boil"
	"github.com/volatiletech/sqlboiler/v4/queries"
	"github.com/volatiletech/strmangle"
)

var (
	// Relationships sometimes use the reflection helper queries.Equal/queries.Assign
	// so force a package dependency in case they don't.
	_ = queries.Equal
)

func testCatalogBrands(t *testing.T) {
	t.Parallel()

	query := CatalogBrands()

	if query.Query == nil {
		t.Error("expected a query, got nothing")
	}
}

func testCatalogBrandsDelete(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &CatalogBrand{}
	if err = randomize.Struct(seed, o, catalogBrandDBTypes, true, catalogBrandColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize CatalogBrand struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if rowsAff, err := o.Delete(ctx, tx); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("should only have deleted one row, but affected:", rowsAff)
	}

	count, err := CatalogBrands().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 0 {
		t.Error("want zero records, got:", count)
	}
}

func testCatalogBrandsQueryDeleteAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &CatalogBrand{}
	if err = randomize.Struct(seed, o, catalogBrandDBTypes, true, catalogBrandColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize CatalogBrand struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if rowsAff, err := CatalogBrands().DeleteAll(ctx, tx); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("should only have deleted one row, but affected:", rowsAff)
	}

	count, err := CatalogBrands().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 0 {
		t.Error("want zero records, got:", count)
	}
}

func testCatalogBrandsSliceDeleteAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &CatalogBrand{}
	if err = randomize.Struct(seed, o, catalogBrandDBTypes, true, catalogBrandColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize CatalogBrand struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	slice := CatalogBrandSlice{o}

	if rowsAff, err := slice.DeleteAll(ctx, tx); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("should only have deleted one row, but affected:", rowsAff)
	}

	count, err := CatalogBrands().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 0 {
		t.Error("want zero records, got:", count)
	}
}

func testCatalogBrandsExists(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &CatalogBrand{}
	if err = randomize.Struct(seed, o, catalogBrandDBTypes, true, catalogBrandColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize CatalogBrand struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	e, err := CatalogBrandExists(ctx, tx, o.ID)
	if err != nil {
		t.Errorf("Unable to check if CatalogBrand exists: %s", err)
	}
	if !e {
		t.Errorf("Expected CatalogBrandExists to return true, but got false.")
	}
}

func testCatalogBrandsFind(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &CatalogBrand{}
	if err = randomize.Struct(seed, o, catalogBrandDBTypes, true, catalogBrandColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize CatalogBrand struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	catalogBrandFound, err := FindCatalogBrand(ctx, tx, o.ID)
	if err != nil {
		t.Error(err)
	}

	if catalogBrandFound == nil {
		t.Error("want a record, got nil")
	}
}

func testCatalogBrandsBind(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &CatalogBrand{}
	if err = randomize.Struct(seed, o, catalogBrandDBTypes, true, catalogBrandColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize CatalogBrand struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if err = CatalogBrands().Bind(ctx, tx, o); err != nil {
		t.Error(err)
	}
}

func testCatalogBrandsOne(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &CatalogBrand{}
	if err = randomize.Struct(seed, o, catalogBrandDBTypes, true, catalogBrandColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize CatalogBrand struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if x, err := CatalogBrands().One(ctx, tx); err != nil {
		t.Error(err)
	} else if x == nil {
		t.Error("expected to get a non nil record")
	}
}

func testCatalogBrandsAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	catalogBrandOne := &CatalogBrand{}
	catalogBrandTwo := &CatalogBrand{}
	if err = randomize.Struct(seed, catalogBrandOne, catalogBrandDBTypes, false, catalogBrandColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize CatalogBrand struct: %s", err)
	}
	if err = randomize.Struct(seed, catalogBrandTwo, catalogBrandDBTypes, false, catalogBrandColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize CatalogBrand struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = catalogBrandOne.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}
	if err = catalogBrandTwo.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	slice, err := CatalogBrands().All(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if len(slice) != 2 {
		t.Error("want 2 records, got:", len(slice))
	}
}

func testCatalogBrandsCount(t *testing.T) {
	t.Parallel()

	var err error
	seed := randomize.NewSeed()
	catalogBrandOne := &CatalogBrand{}
	catalogBrandTwo := &CatalogBrand{}
	if err = randomize.Struct(seed, catalogBrandOne, catalogBrandDBTypes, false, catalogBrandColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize CatalogBrand struct: %s", err)
	}
	if err = randomize.Struct(seed, catalogBrandTwo, catalogBrandDBTypes, false, catalogBrandColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize CatalogBrand struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = catalogBrandOne.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}
	if err = catalogBrandTwo.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	count, err := CatalogBrands().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 2 {
		t.Error("want 2 records, got:", count)
	}
}

func catalogBrandBeforeInsertHook(ctx context.Context, e boil.ContextExecutor, o *CatalogBrand) error {
	*o = CatalogBrand{}
	return nil
}

func catalogBrandAfterInsertHook(ctx context.Context, e boil.ContextExecutor, o *CatalogBrand) error {
	*o = CatalogBrand{}
	return nil
}

func catalogBrandAfterSelectHook(ctx context.Context, e boil.ContextExecutor, o *CatalogBrand) error {
	*o = CatalogBrand{}
	return nil
}

func catalogBrandBeforeUpdateHook(ctx context.Context, e boil.ContextExecutor, o *CatalogBrand) error {
	*o = CatalogBrand{}
	return nil
}

func catalogBrandAfterUpdateHook(ctx context.Context, e boil.ContextExecutor, o *CatalogBrand) error {
	*o = CatalogBrand{}
	return nil
}

func catalogBrandBeforeDeleteHook(ctx context.Context, e boil.ContextExecutor, o *CatalogBrand) error {
	*o = CatalogBrand{}
	return nil
}

func catalogBrandAfterDeleteHook(ctx context.Context, e boil.ContextExecutor, o *CatalogBrand) error {
	*o = CatalogBrand{}
	return nil
}

func catalogBrandBeforeUpsertHook(ctx context.Context, e boil.ContextExecutor, o *CatalogBrand) error {
	*o = CatalogBrand{}
	return nil
}

func catalogBrandAfterUpsertHook(ctx context.Context, e boil.ContextExecutor, o *CatalogBrand) error {
	*o = CatalogBrand{}
	return nil
}

func testCatalogBrandsHooks(t *testing.T) {
	t.Parallel()

	var err error

	ctx := context.Background()
	empty := &CatalogBrand{}
	o := &CatalogBrand{}

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, o, catalogBrandDBTypes, false); err != nil {
		t.Errorf("Unable to randomize CatalogBrand object: %s", err)
	}

	AddCatalogBrandHook(boil.BeforeInsertHook, catalogBrandBeforeInsertHook)
	if err = o.doBeforeInsertHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doBeforeInsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeInsertHook function to empty object, but got: %#v", o)
	}
	catalogBrandBeforeInsertHooks = []CatalogBrandHook{}

	AddCatalogBrandHook(boil.AfterInsertHook, catalogBrandAfterInsertHook)
	if err = o.doAfterInsertHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterInsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterInsertHook function to empty object, but got: %#v", o)
	}
	catalogBrandAfterInsertHooks = []CatalogBrandHook{}

	AddCatalogBrandHook(boil.AfterSelectHook, catalogBrandAfterSelectHook)
	if err = o.doAfterSelectHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterSelectHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterSelectHook function to empty object, but got: %#v", o)
	}
	catalogBrandAfterSelectHooks = []CatalogBrandHook{}

	AddCatalogBrandHook(boil.BeforeUpdateHook, catalogBrandBeforeUpdateHook)
	if err = o.doBeforeUpdateHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doBeforeUpdateHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeUpdateHook function to empty object, but got: %#v", o)
	}
	catalogBrandBeforeUpdateHooks = []CatalogBrandHook{}

	AddCatalogBrandHook(boil.AfterUpdateHook, catalogBrandAfterUpdateHook)
	if err = o.doAfterUpdateHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterUpdateHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterUpdateHook function to empty object, but got: %#v", o)
	}
	catalogBrandAfterUpdateHooks = []CatalogBrandHook{}

	AddCatalogBrandHook(boil.BeforeDeleteHook, catalogBrandBeforeDeleteHook)
	if err = o.doBeforeDeleteHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doBeforeDeleteHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeDeleteHook function to empty object, but got: %#v", o)
	}
	catalogBrandBeforeDeleteHooks = []CatalogBrandHook{}

	AddCatalogBrandHook(boil.AfterDeleteHook, catalogBrandAfterDeleteHook)
	if err = o.doAfterDeleteHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterDeleteHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterDeleteHook function to empty object, but got: %#v", o)
	}
	catalogBrandAfterDeleteHooks = []CatalogBrandHook{}

	AddCatalogBrandHook(boil.BeforeUpsertHook, catalogBrandBeforeUpsertHook)
	if err = o.doBeforeUpsertHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doBeforeUpsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeUpsertHook function to empty object, but got: %#v", o)
	}
	catalogBrandBeforeUpsertHooks = []CatalogBrandHook{}

	AddCatalogBrandHook(boil.AfterUpsertHook, catalogBrandAfterUpsertHook)
	if err = o.doAfterUpsertHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterUpsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterUpsertHook function to empty object, but got: %#v", o)
	}
	catalogBrandAfterUpsertHooks = []CatalogBrandHook{}
}

func testCatalogBrandsInsert(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &CatalogBrand{}
	if err = randomize.Struct(seed, o, catalogBrandDBTypes, true, catalogBrandColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize CatalogBrand struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	count, err := CatalogBrands().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}
}

func testCatalogBrandsInsertWhitelist(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &CatalogBrand{}
	if err = randomize.Struct(seed, o, catalogBrandDBTypes, true); err != nil {
		t.Errorf("Unable to randomize CatalogBrand struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Whitelist(catalogBrandColumnsWithoutDefault...)); err != nil {
		t.Error(err)
	}

	count, err := CatalogBrands().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}
}

func testCatalogBrandToManyCatalogItems(t *testing.T) {
	var err error
	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a CatalogBrand
	var b, c CatalogItem

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, catalogBrandDBTypes, true, catalogBrandColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize CatalogBrand struct: %s", err)
	}

	if err := a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	if err = randomize.Struct(seed, &b, catalogItemDBTypes, false, catalogItemColumnsWithDefault...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &c, catalogItemDBTypes, false, catalogItemColumnsWithDefault...); err != nil {
		t.Fatal(err)
	}

	queries.Assign(&b.CatalogBrandID, a.ID)
	queries.Assign(&c.CatalogBrandID, a.ID)
	if err = b.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = c.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	check, err := a.CatalogItems().All(ctx, tx)
	if err != nil {
		t.Fatal(err)
	}

	bFound, cFound := false, false
	for _, v := range check {
		if queries.Equal(v.CatalogBrandID, b.CatalogBrandID) {
			bFound = true
		}
		if queries.Equal(v.CatalogBrandID, c.CatalogBrandID) {
			cFound = true
		}
	}

	if !bFound {
		t.Error("expected to find b")
	}
	if !cFound {
		t.Error("expected to find c")
	}

	slice := CatalogBrandSlice{&a}
	if err = a.L.LoadCatalogItems(ctx, tx, false, (*[]*CatalogBrand)(&slice), nil); err != nil {
		t.Fatal(err)
	}
	if got := len(a.R.CatalogItems); got != 2 {
		t.Error("number of eager loaded records wrong, got:", got)
	}

	a.R.CatalogItems = nil
	if err = a.L.LoadCatalogItems(ctx, tx, true, &a, nil); err != nil {
		t.Fatal(err)
	}
	if got := len(a.R.CatalogItems); got != 2 {
		t.Error("number of eager loaded records wrong, got:", got)
	}

	if t.Failed() {
		t.Logf("%#v", check)
	}
}

func testCatalogBrandToManyAddOpCatalogItems(t *testing.T) {
	var err error

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a CatalogBrand
	var b, c, d, e CatalogItem

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, catalogBrandDBTypes, false, strmangle.SetComplement(catalogBrandPrimaryKeyColumns, catalogBrandColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	foreigners := []*CatalogItem{&b, &c, &d, &e}
	for _, x := range foreigners {
		if err = randomize.Struct(seed, x, catalogItemDBTypes, false, strmangle.SetComplement(catalogItemPrimaryKeyColumns, catalogItemColumnsWithoutDefault)...); err != nil {
			t.Fatal(err)
		}
	}

	if err := a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = b.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = c.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	foreignersSplitByInsertion := [][]*CatalogItem{
		{&b, &c},
		{&d, &e},
	}

	for i, x := range foreignersSplitByInsertion {
		err = a.AddCatalogItems(ctx, tx, i != 0, x...)
		if err != nil {
			t.Fatal(err)
		}

		first := x[0]
		second := x[1]

		if !queries.Equal(a.ID, first.CatalogBrandID) {
			t.Error("foreign key was wrong value", a.ID, first.CatalogBrandID)
		}
		if !queries.Equal(a.ID, second.CatalogBrandID) {
			t.Error("foreign key was wrong value", a.ID, second.CatalogBrandID)
		}

		if first.R.CatalogBrand != &a {
			t.Error("relationship was not added properly to the foreign slice")
		}
		if second.R.CatalogBrand != &a {
			t.Error("relationship was not added properly to the foreign slice")
		}

		if a.R.CatalogItems[i*2] != first {
			t.Error("relationship struct slice not set to correct value")
		}
		if a.R.CatalogItems[i*2+1] != second {
			t.Error("relationship struct slice not set to correct value")
		}

		count, err := a.CatalogItems().Count(ctx, tx)
		if err != nil {
			t.Fatal(err)
		}
		if want := int64((i + 1) * 2); count != want {
			t.Error("want", want, "got", count)
		}
	}
}

func testCatalogBrandToManySetOpCatalogItems(t *testing.T) {
	var err error

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a CatalogBrand
	var b, c, d, e CatalogItem

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, catalogBrandDBTypes, false, strmangle.SetComplement(catalogBrandPrimaryKeyColumns, catalogBrandColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	foreigners := []*CatalogItem{&b, &c, &d, &e}
	for _, x := range foreigners {
		if err = randomize.Struct(seed, x, catalogItemDBTypes, false, strmangle.SetComplement(catalogItemPrimaryKeyColumns, catalogItemColumnsWithoutDefault)...); err != nil {
			t.Fatal(err)
		}
	}

	if err = a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = b.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = c.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	err = a.SetCatalogItems(ctx, tx, false, &b, &c)
	if err != nil {
		t.Fatal(err)
	}

	count, err := a.CatalogItems().Count(ctx, tx)
	if err != nil {
		t.Fatal(err)
	}
	if count != 2 {
		t.Error("count was wrong:", count)
	}

	err = a.SetCatalogItems(ctx, tx, true, &d, &e)
	if err != nil {
		t.Fatal(err)
	}

	count, err = a.CatalogItems().Count(ctx, tx)
	if err != nil {
		t.Fatal(err)
	}
	if count != 2 {
		t.Error("count was wrong:", count)
	}

	if !queries.IsValuerNil(b.CatalogBrandID) {
		t.Error("want b's foreign key value to be nil")
	}
	if !queries.IsValuerNil(c.CatalogBrandID) {
		t.Error("want c's foreign key value to be nil")
	}
	if !queries.Equal(a.ID, d.CatalogBrandID) {
		t.Error("foreign key was wrong value", a.ID, d.CatalogBrandID)
	}
	if !queries.Equal(a.ID, e.CatalogBrandID) {
		t.Error("foreign key was wrong value", a.ID, e.CatalogBrandID)
	}

	if b.R.CatalogBrand != nil {
		t.Error("relationship was not removed properly from the foreign struct")
	}
	if c.R.CatalogBrand != nil {
		t.Error("relationship was not removed properly from the foreign struct")
	}
	if d.R.CatalogBrand != &a {
		t.Error("relationship was not added properly to the foreign struct")
	}
	if e.R.CatalogBrand != &a {
		t.Error("relationship was not added properly to the foreign struct")
	}

	if a.R.CatalogItems[0] != &d {
		t.Error("relationship struct slice not set to correct value")
	}
	if a.R.CatalogItems[1] != &e {
		t.Error("relationship struct slice not set to correct value")
	}
}

func testCatalogBrandToManyRemoveOpCatalogItems(t *testing.T) {
	var err error

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a CatalogBrand
	var b, c, d, e CatalogItem

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, catalogBrandDBTypes, false, strmangle.SetComplement(catalogBrandPrimaryKeyColumns, catalogBrandColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	foreigners := []*CatalogItem{&b, &c, &d, &e}
	for _, x := range foreigners {
		if err = randomize.Struct(seed, x, catalogItemDBTypes, false, strmangle.SetComplement(catalogItemPrimaryKeyColumns, catalogItemColumnsWithoutDefault)...); err != nil {
			t.Fatal(err)
		}
	}

	if err := a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	err = a.AddCatalogItems(ctx, tx, true, foreigners...)
	if err != nil {
		t.Fatal(err)
	}

	count, err := a.CatalogItems().Count(ctx, tx)
	if err != nil {
		t.Fatal(err)
	}
	if count != 4 {
		t.Error("count was wrong:", count)
	}

	err = a.RemoveCatalogItems(ctx, tx, foreigners[:2]...)
	if err != nil {
		t.Fatal(err)
	}

	count, err = a.CatalogItems().Count(ctx, tx)
	if err != nil {
		t.Fatal(err)
	}
	if count != 2 {
		t.Error("count was wrong:", count)
	}

	if !queries.IsValuerNil(b.CatalogBrandID) {
		t.Error("want b's foreign key value to be nil")
	}
	if !queries.IsValuerNil(c.CatalogBrandID) {
		t.Error("want c's foreign key value to be nil")
	}

	if b.R.CatalogBrand != nil {
		t.Error("relationship was not removed properly from the foreign struct")
	}
	if c.R.CatalogBrand != nil {
		t.Error("relationship was not removed properly from the foreign struct")
	}
	if d.R.CatalogBrand != &a {
		t.Error("relationship to a should have been preserved")
	}
	if e.R.CatalogBrand != &a {
		t.Error("relationship to a should have been preserved")
	}

	if len(a.R.CatalogItems) != 2 {
		t.Error("should have preserved two relationships")
	}

	// Removal doesn't do a stable deletion for performance so we have to flip the order
	if a.R.CatalogItems[1] != &d {
		t.Error("relationship to d should have been preserved")
	}
	if a.R.CatalogItems[0] != &e {
		t.Error("relationship to e should have been preserved")
	}
}

func testCatalogBrandsReload(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &CatalogBrand{}
	if err = randomize.Struct(seed, o, catalogBrandDBTypes, true, catalogBrandColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize CatalogBrand struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if err = o.Reload(ctx, tx); err != nil {
		t.Error(err)
	}
}

func testCatalogBrandsReloadAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &CatalogBrand{}
	if err = randomize.Struct(seed, o, catalogBrandDBTypes, true, catalogBrandColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize CatalogBrand struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	slice := CatalogBrandSlice{o}

	if err = slice.ReloadAll(ctx, tx); err != nil {
		t.Error(err)
	}
}

func testCatalogBrandsSelect(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &CatalogBrand{}
	if err = randomize.Struct(seed, o, catalogBrandDBTypes, true, catalogBrandColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize CatalogBrand struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	slice, err := CatalogBrands().All(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if len(slice) != 1 {
		t.Error("want one record, got:", len(slice))
	}
}

var (
	catalogBrandDBTypes = map[string]string{`ID`: `integer`, `Name`: `character varying`}
	_                   = bytes.MinRead
)

func testCatalogBrandsUpdate(t *testing.T) {
	t.Parallel()

	if 0 == len(catalogBrandPrimaryKeyColumns) {
		t.Skip("Skipping table with no primary key columns")
	}
	if len(catalogBrandAllColumns) == len(catalogBrandPrimaryKeyColumns) {
		t.Skip("Skipping table with only primary key columns")
	}

	seed := randomize.NewSeed()
	var err error
	o := &CatalogBrand{}
	if err = randomize.Struct(seed, o, catalogBrandDBTypes, true, catalogBrandColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize CatalogBrand struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	count, err := CatalogBrands().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}

	if err = randomize.Struct(seed, o, catalogBrandDBTypes, true, catalogBrandPrimaryKeyColumns...); err != nil {
		t.Errorf("Unable to randomize CatalogBrand struct: %s", err)
	}

	if rowsAff, err := o.Update(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("should only affect one row but affected", rowsAff)
	}
}

func testCatalogBrandsSliceUpdateAll(t *testing.T) {
	t.Parallel()

	if len(catalogBrandAllColumns) == len(catalogBrandPrimaryKeyColumns) {
		t.Skip("Skipping table with only primary key columns")
	}

	seed := randomize.NewSeed()
	var err error
	o := &CatalogBrand{}
	if err = randomize.Struct(seed, o, catalogBrandDBTypes, true, catalogBrandColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize CatalogBrand struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	count, err := CatalogBrands().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}

	if err = randomize.Struct(seed, o, catalogBrandDBTypes, true, catalogBrandPrimaryKeyColumns...); err != nil {
		t.Errorf("Unable to randomize CatalogBrand struct: %s", err)
	}

	// Remove Primary keys and unique columns from what we plan to update
	var fields []string
	if strmangle.StringSliceMatch(catalogBrandAllColumns, catalogBrandPrimaryKeyColumns) {
		fields = catalogBrandAllColumns
	} else {
		fields = strmangle.SetComplement(
			catalogBrandAllColumns,
			catalogBrandPrimaryKeyColumns,
		)
	}

	value := reflect.Indirect(reflect.ValueOf(o))
	typ := reflect.TypeOf(o).Elem()
	n := typ.NumField()

	updateMap := M{}
	for _, col := range fields {
		for i := 0; i < n; i++ {
			f := typ.Field(i)
			if f.Tag.Get("boil") == col {
				updateMap[col] = value.Field(i).Interface()
			}
		}
	}

	slice := CatalogBrandSlice{o}
	if rowsAff, err := slice.UpdateAll(ctx, tx, updateMap); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("wanted one record updated but got", rowsAff)
	}
}

func testCatalogBrandsUpsert(t *testing.T) {
	t.Parallel()

	if len(catalogBrandAllColumns) == len(catalogBrandPrimaryKeyColumns) {
		t.Skip("Skipping table with only primary key columns")
	}

	seed := randomize.NewSeed()
	var err error
	// Attempt the INSERT side of an UPSERT
	o := CatalogBrand{}
	if err = randomize.Struct(seed, &o, catalogBrandDBTypes, true); err != nil {
		t.Errorf("Unable to randomize CatalogBrand struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Upsert(ctx, tx, false, nil, boil.Infer(), boil.Infer()); err != nil {
		t.Errorf("Unable to upsert CatalogBrand: %s", err)
	}

	count, err := CatalogBrands().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}
	if count != 1 {
		t.Error("want one record, got:", count)
	}

	// Attempt the UPDATE side of an UPSERT
	if err = randomize.Struct(seed, &o, catalogBrandDBTypes, false, catalogBrandPrimaryKeyColumns...); err != nil {
		t.Errorf("Unable to randomize CatalogBrand struct: %s", err)
	}

	if err = o.Upsert(ctx, tx, true, nil, boil.Infer(), boil.Infer()); err != nil {
		t.Errorf("Unable to upsert CatalogBrand: %s", err)
	}

	count, err = CatalogBrands().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}
	if count != 1 {
		t.Error("want one record, got:", count)
	}
}
